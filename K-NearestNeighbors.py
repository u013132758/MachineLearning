from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.neighbors import KNeighborsClassifier
import numpy as np
import matplotlib.pyplot as plt

# 加载鸢尾花数据集
iris = load_iris()
X = iris.data
y = iris.target

# //////////////////k 折交叉验证选择 K 值的代码示例///////////////////
# 划分训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# # 初始化K值范围
# k_values = range(1, 31)
# cv_scores = []

# # 进行k折交叉验证（这里使用5折交叉验证）
# for k in k_values:
#     knn = KNeighborsClassifier(n_neighbors=k)
#     scores = cross_val_score(knn, X_train, y_train, cv=5, scoring='accuracy')
#     cv_scores.append(scores.mean())

# # 找到最优K值
# best_k = k_values[np.argmax(cv_scores)]
# print(f"最优的K值为: {best_k}")

# 绘制K值与交叉验证得分的关系图
# plt.rcParams['font.family'] = ['sans-serif']
# plt.rcParams['font.sans-serif'] = ['Arial Unicode MS']
# plt.rcParams['axes.unicode_minus'] = False
# plt.plot(k_values, cv_scores)
# plt.xlabel('K值')
# plt.ylabel('交叉验证平均准确率')
# plt.title('K值对交叉验证准确率的影响')
# plt.show()
# /////////////////////////////////////////////////////////////////

# //////////////////////////////////////
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

# 初始化KNN分类器，这里设置K值为5

knn = KNeighborsClassifier(n_neighbors=5)

# 使用训练集训练模型

knn.fit(X_train, y_train)

# 使用训练好的模型对测试集进行预测

y_pred = knn.predict(X_test)

# 计算模型在测试集上的准确率

accuracy = accuracy_score(y_test, y_pred)

print(f"模型的准确率为: {accuracy}")

# 生成分类报告，包含精确率、召回率、F1值等指标

print("分类报告:\n", classification_report(y_test, y_pred))

# 生成混淆矩阵

print("混淆矩阵:\n", confusion_matrix(y_test, y_pred))


# /////////////////////////////////////////////////////////////////
