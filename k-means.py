from sklearn.datasets import load_iris
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import metrics
# 加载Iris数据集

iris = load_iris()

data = iris.data

# 数据标准化

scaler = StandardScaler()

data_scaled = scaler.fit_transform(data)
# 初始化K-平均模型，设置簇的数量为3

kmeans = KMeans(n_clusters=3, random_state=42)
# 训练模型

kmeans.fit(data_scaled)
# 预测每个数据点所属的簇

labels = kmeans.predict(data_scaled)
# 查看每个簇的样本数量

cluster_sizes = np.unique(labels, return_counts=True)[1]

print("每个簇的样本数量:", cluster_sizes)

# 查看每个簇的质心

cluster_centers = kmeans.cluster_centers_

print("每个簇的质心:\n", cluster_centers)

# 选择花萼长度和花瓣长度两个特征

x = data[:, 0]

y = data[:, 2]

sse = kmeans.inertia_
print("SSE:", sse)
# 计算轮廓系数

silhouette_score = metrics.silhouette_score(data_scaled, labels)

print("轮廓系数:", silhouette_score)

# 绘制散点图，不同颜色表示不同的簇

plt.scatter(x, y, c=labels, cmap='viridis')



# 绘制簇的质心
plt.rcParams['font.family'] = ['sans-serif']
plt.rcParams['font.sans-serif'] = ['Arial Unicode MS']
plt.rcParams['axes.unicode_minus'] = False
plt.scatter(cluster_centers[:, 0], cluster_centers[:, 2], marker='x', c='red', s=200)

plt.xlabel('花萼长度')

plt.ylabel('花瓣长度')

plt.title('K-平均算法聚类结果')

plt.show()



